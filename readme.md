# My Cloud Notes (Backend)

This is the back end application for My Cloud Notes App.

## API

### User register [POST]

** URL: api/register **

| Name     | Type   | Description |
|----------|--------|-------------|
| name     | string | Full name   |
| email    | string | e-mail      |
| password | string | password    |


### User login [POST]

** URL: api/login **

| Name     | Type   | Description |
|----------|--------|-------------|
| email    | string | e-mail      |
| password | string | password    |

### New  note [POST]

** URL: api/v1/notes **

| Name     | Type   | Description |
|----------|--------|-------------|
| api_key  | string | API Key     |
| title    | string | Title       |
| body     | string | Body        |
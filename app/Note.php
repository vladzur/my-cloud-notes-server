<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Note extends Model
{
    public $incrementing = false;
    protected $fillable = ['title', 'body'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::uuid4()->toString();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

<?php

Route::group(['middleware' => 'web'], function() {

    Route::auth();

    Route::get('/home', 'HomeController@index');
});

Route::group(['middleware' => 'api'], function() {

    Route::post('api/login', 'UsersController@login');

    Route::post('api/register', 'UsersController@register');
});

Route::group(['middleware' => ['api', 'auth:api']], function () {

    Route::resource('api/v1/notes', 'NotesController');

});

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    public function register(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->api_token = str_random(60);
        if (!$user->save()) {
            return response('Unauthorized.', 401);
        }
        return response()->json(['apt_token' => $user->api_token]);
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json(['api_token' => Auth::user()->api_token]);
        }
        return response('Unauthorized.', 401);
    }
}
